import copy
import random

import numpy as np

N_INOUT_MISMATCH = -1

class NeuralNetwork:
    """Implementation of an artifical neural network.

    Note
    ----
    The first layer (layer 0) corresponds to the input
    layer, and the last layer corresponds to the output
    layer.
    For every layer but the last, the first neuron is
    the bias neuron. The last layer doesn't have a bias
    neuron.

    Attributes
    ----------
    weight : list of numpy matrices
        Store weights between neurons. weight[l][i, j]
        corresponds to the weight from the neuron 'j' in
        layer 'l' to neuron 'i' in layer 'l + 1'.

    activation : list of numpy arrays
        Store outputs (activation) from neurons.
        activation[l][i] corresponds to output from
        neuron 'i' in layer 'l'.

    delta : list of numpy arrays
        Store deltas from neurons.
        delta[l][i] corresponds to delta from neuron
        'i' in layer 'l'. i = 0 is the bias neuron,
        except in output layer.

    size_layers : list of integers
        Number of neurons for each layer. [1,2,3] means
        the first layer has one neuron, second has two
         neurons and third (output) has three neurons.

    lambda_reg : number
        Regularization term coefficient.

    alfa : number
        Learning rate coefficient.
    """

    def __init__(self, size_layers):
        """Initialize neural network.

        Parameters
        ----------
        size_layers : list
            Number of neurons for each layer.
            First layer is the input layer last is the
            output layer.
            [1,2,3] means the first layer has one neuron,
            second has two and third (output) has three.
        """
        self.weight = []
        self.activation = []
        self.delta = []
        self.size_layers = size_layers
        self.lambda_reg = 0.1
        self.alfa = 0.1
        self.start_weights()
        self.start_activations()
        self.start_deltas()

    def g_func(self, vector):
        """Currently uses sigmoid function for all neurons. """
        return 1/(1 + np.exp(-1 * vector))
        #return vector

    def start_weights(self):
        """Start weights according to the size of each layer."""
        size_layers = self.size_layers
        for layer in range(len(size_layers) - 1):
            weights = np.random.random(
                (size_layers[layer + 1], size_layers[layer] + 1)
                ) + 0.0
            self.weight.append(weights)    # Adds bias and avoids 0 valued weight.

    def start_activations(self):
        """Start activation arrays according to the size of each layer."""
        size_layers = self.size_layers
        for layer in range(len(size_layers) - 1):
            self.activation.append(np.zeros(size_layers[layer] + 1))
            self.activation[layer][0] = 1  # Adds bias neuron.
        output_layer = len(size_layers) - 1
        self.activation.append(np.zeros(size_layers[output_layer]))

    def start_deltas(self):
        """Start delta structure with zeros.
        Start delta for bias neuron too, even though it won't be used.
        """
        size_layers = self.size_layers
        for layer in range(len(size_layers) - 1):
            self.delta.append(np.zeros(size_layers[layer] + 1)) # Includes bias
        output_layer = len(size_layers) - 1
        self.delta.append(np.zeros(size_layers[output_layer])) # No bias in last layer


    def propagate(self, features):
        """Propagate input through neural network.

        Parameters
        ----------
        features : list
            One instance for propagation. List of
            normalized feature values.

        Return
        ------
        Numpy array of outputs from output layer.
        """
        size_layers = self.size_layers
        layers = len(size_layers)
        self.activation[0][1:] = np.array(features) # first neuron is the bias
        for layer in range(1, layers - 1):
            self.activation[layer][1:] = self.g_func(
                self.weight[layer - 1].dot(self.activation[layer - 1]))

        # Last layer doesn't have bias neuron
        self.activation[layers - 1] = self.g_func(self.weight[layers - 2]
                                                  .dot(self.activation[layers - 2]))
        return self.activation[layers - 1]

    def delta_backpropagate(self, output_correct):
        """Backpropagate error to find delta for each neuron.
        Uses activation list to calculate values, hence propagate must
        be called first.

        Parameters
        ----------
        output_correct : list
            List of correct output values for features used
            in propagate method.
        """
        size_layers = self.size_layers
        last = len(size_layers) - 1
        for neuron in range(size_layers[last]): # Calculate delta for output layer
            self.delta[last][neuron] = self.activation[last][neuron] - output_correct[neuron]

        # Calculate delta for each neuron as a matrix multiplication, including
        # the bias neuron, even though it is not necessary, it makes implementation
        # more straight forward.

        # Last layer doesn't have bias
        layer = last - 1
        self.delta[layer] = self.weight[layer].transpose().dot(self.delta[layer + 1])
        self.delta[layer] = self.delta[layer]*(self.activation[layer]*(1 - self.activation[layer]))

        for layer in range(last - 2, -1, -1):
            self.delta[layer] = self.weight[layer].transpose().dot(self.delta[layer + 1][1:])
            self.delta[layer] = self.delta[layer]*(self.activation[layer]*(1 - self.activation[layer]))

    def gradient(self, layer, i, j):
        """ Calculate gradiente for w[l][i, j] """
        last = len(self.size_layers) - 1

        # Delta indexes for layers with bias neurons have index 0 for bias
        if layer + 1 == last:
            grad = self.activation[layer][j] * self.delta[layer + 1][i]
        else:
            grad = self.activation[layer][j] * self.delta[layer + 1][i + 1]

        if layer != last and j == 0: # Bias neuron doesn't get regularized
            return grad

        grad += self.lambda_reg * self.weight[layer][i, j]

        return grad

    def update_weights(self):
        """ Update weights using present deltas and activations.
        Must be called after delta_backpropagate().
        """
        layers_total = len(self.size_layers)
        for layer in range(layers_total - 1): # Last layer doesn't have weights
            shape = self.weight[layer].shape
            for i in range(shape[0]):
                for j in range(shape[1]):
                    grad = self.gradient(layer, i, j)
                    self.weight[layer][i, j] -= self.alfa * grad

    def regularization(self):
        """ Calculate regularization term for current weights. """
        layers_total = len(self.size_layers)
        sum_reg = 0
        for layer in range(layers_total - 1): # Last layer doesn't have weights
            aux_sum = np.sum(self.weight[layer]**2, axis=0)
            sum_reg += np.sum(aux_sum[1:]) # First column has only bias neurons,
                                           # hence it isn't summed.

        return sum_reg


    def cost(self, out_predicted, out_correct):
        """ Apply a choosen cost function to all outputs from
        output layer.
        """
        y = out_correct
        x = out_predicted
        return -1*np.sum(y * np.log(x) + (1 - y) * np.log(1 - x))

    def error_j(self, inputs, outputs):
        """Calculate total cost from network.

        Parameters
        ----------
        inputs : list of lists
            Input instances used for training
            the network.

        outputs : list of lists
            The expected outputs for the inputs
            given.

        Return
        ------
        A float number representing the cost.
        """
        cost = 0

        #Number of input instances must be the same as output
        if len(outputs) != len(inputs):
            return N_INOUT_MISMATCH

        outputs = np.array(outputs)
        for i in range(0, len(outputs)):
            activation = self.propagate(inputs[i])
            cost += self.cost(activation, outputs[i])

        cost += 0.5 * self.lambda_reg * self.regularization()
	
        n = len(outputs)

        return cost/n

    def train_backprop(self, inp, out):
        """Update weights through backpropagation for one example.

        Parameters
        ----------
        inp : list
            Normalized input features for propagation.

        out : list
            Expected output values for given input.

        Examples
        --------
        backpropagation([1, 2, 3], [2, 4, 6])
        """
        self.propagate(inp)
        self.delta_backpropagate(out)
        self.update_weights()

    def approximate_weights(self, inp, out, get_grads = False):
        """Calculate weights through numerical methods for gradient approximation.

        Parameters
        ----------
        inp : list
            One instance for propagation. List of normalized feature values.

        out : list
            List of expected output values for given input.

        Return
        ------
        Return estimated gradients for each weight.
        """
        gradients = []
        eps = 0.000001
        layers_total = len(self.size_layers)
        for l in range(layers_total - 1): # Last layer doesn't have weights
            shape = self.weight[l].shape
            gradients.append(np.zeros(shape))
            for i in range(shape[0]):
                for j in range(shape[1]):
                    self.weight[l][i, j] += eps
                    f1 = self.error_j([inp], [out])
                    self.weight[l][i, j] -= 2*eps
                    f2 = self.error_j([inp], [out])
                    grad = (f1 - f2)/(2*eps)
                    gradients[l][i, j] = grad

                    self.weight[l][i, j] += eps
                    if get_grads is False:
                        self.weight[l][i, j] -= self.alfa * grad

        return gradients

def calc_gradients(neural_net):
    """Calculate gradients for each weight based on
    deltas from given neural network.

    Parameters
    ----------
    neural_net : NeuralNetwork
        Neural network whose deltas will be used to find
        gradients.

    Return
    ------
    Calculated gradients for each weight.
    """
    gradients = []
    layers_total = len(neural_net.size_layers)
    for layer in range(layers_total - 1): # Last layer doesn't have weights
        shape = neural_net.weight[layer].shape
        gradients.append(np.zeros(shape))
        for i in range(shape[0]):
            for j in range(shape[1]):
                grad = neural_net.gradient(layer, i, j)
                gradients[layer][i, j] = grad

    return gradients           

def verify_gradient():
    """Create neural network with one hidden layer containing two neurons
    and compare updated weights obtained through backpropagation and through
    numerical approximation.
    """
    alfa = 0.1
    lambda_reg = 0
    inp = [0.8] # Any value between 0 and 1 would serve.
    out = [1] # Any value between 0 and 1 would serve.
    shape = [1, 2, 1]

    nn_test = NeuralNetwork(shape)
    nn_test.alfa = alfa
    nn_test.lambda_reg = lambda_reg
    weights = copy.deepcopy(nn_test.weight)

    nn_test.propagate(inp)
    nn_test.delta_backpropagate(out)
    gradients = calc_gradients(nn_test)

    # Create new neural network with same initial weights
    nn_test = NeuralNetwork(shape)
    nn_test.alfa = alfa
    nn_test.lambda_reg = lambda_reg
    nn_test.weight = copy.deepcopy(weights)

    gradients_appr = nn_test.approximate_weights(inp, out, get_grads = True)

    for l in range(len(shape) - 1): # Last layer doesn't have weights
        shape = nn_test.weight[l].shape
        for i in range(shape[0]):
            for j in range(shape[1]):
                print("Grad[{}][{}, {}]".format(l, i, j))
                rel_diff = (gradients_appr[l][i, j] - gradients[l][i, j]) / (gradients[l][i, j])
                print("Appr: {} Calc: {} Relat. Diff: {}".format(gradients_appr[l][i, j], gradients[l][i, j], rel_diff))

if __name__ == '__main__':
    verify_gradient()