import numpy as np
import re
import random

class Normalization:

	def __init__(self, filename, class_column):
		self.filename = filename
		self.class_column = class_column
		self.class_mapping = dict()
		self.inputs = []
		self.outputs = []
		self.max_attr = []
		self.min_attr = []
		self.count_y = dict()

	def map_classes(self, classes):
		"""
			in multi-class problems, we don't know how the classes are apresented,
			then we need to map a class to a vector like [0, 0, 1] (for the
			third class apresented)
		"""

		classes.sort()
		
		for i in range(0, len(classes)):
			self.class_mapping[classes[i]] = i


	def extract_file(self):
		"""
		Read the file, record the max and min value for each
		attribute and transform the text in two lists (inputs and outputs)

		Return
		------
		The instances and their respective classes separately
		"""
		classes = []
		list_instances = []

		file = open(self.filename, "r")
		
		first_instance = file.readline().replace("\n", "").split(",")

		key = 0

		for i in range(len(first_instance)):
			if i != self.class_column:
				self.max_attr.append(float(first_instance[i]))
				self.min_attr.append(float(first_instance[i]))

		file.seek(0)
		instances = file.readlines()

		random.shuffle(instances)

		for instance in instances:

			if instance == '\n':
				continue

			instance_array = []
			
			for attr in instance.split(","):
				instance_array.append(attr.replace("\n", ""))
			
			if instance_array[self.class_column] not in classes:
				classes.append(instance_array[self.class_column])

			inputs = self.get_inputs_from_instance(instance_array)

			self.inputs.append(inputs)
			self.outputs.append(instance_array[self.class_column])

		self.map_classes(classes)

		file.close()

		return [self.inputs, self.outputs]


	def normalize_instances(self):
		"""
		Normalize the values of the attributes and classes
		of the inputs and outputs list

		Return
		------
		The normalized instances of the given file (at the instantiation)
		"""
		self.extract_file()
		
		for instance in range(0, len(self.inputs)):
			for attr in range(0, len(self.inputs[instance])):
				self.inputs[instance][attr] = self.normalize_input(self.inputs[instance][attr], attr)

			self.outputs[instance] = self.normalize_class(self.outputs[instance])

		return [self.inputs, self.outputs]

	def normalize_class(self, class_):
		"""
		Returns the normalized form of a class of the dataset
		(3 -> [0, 0, 1], for example)
		"""
		norm_class = [0]*len(self.class_mapping)
		norm_class[self.class_mapping[str(class_)]] = 1

		return norm_class

	def normalize_input(self, data, index_attr):
		"""
		Returns the normalized value of an attribute
		"""
		return (data-self.min_attr[index_attr])/(self.max_attr[index_attr] - self.min_attr[index_attr])

	def get_inputs_from_instance(self, instance):
		"""
		Return just what is input in an instance
		"""

		inputs = []
		key = 0
		
		for i in range(0, len(instance)):
			if i != self.class_column:
				if instance[i] == '?':
					instance[i] = 5
				self.max_attr[key] = max(self.max_attr[key], float(instance[i]))
				self.min_attr[key] = min(self.min_attr[key], float(instance[i]))
				inputs.append(float(instance[i]))
				key += 1

		return inputs
