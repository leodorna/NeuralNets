from NeuralNetwork import NeuralNetwork
from Validation import Validation
from Normalization import Normalization
import numpy as np
import matplotlib.pyplot as plt

class Networks:
    """Contains functions with training and evaluation
    of different datasets."""

    def dataset_01(self):
        """Training and evaluation for Haberman's
        Survival Data Set."""
        norm = Normalization("DataSet_01", 3)
        results = norm.normalize_instances()

        inputs = results[0]
        outputs = results[1]

        # shapes = [[3, 3, 1], [3, 6, 1], [3, 3, 3, 1], [3, 6, 3, 1]]
        # alfas = [0.0005, 0.005, 0.05, 0.1]
        # lambdas = [0.000001, 0.00001, 0.0001, 0.001]

        shapes = [[3, 3, 2], [3, 9, 2], [3, 1, 2], [3, 6, 2]]
        alfas = [1, 0.1, 0.01, 0.001]
        lambdas = [0.01, 0.001, 0]
        grid = [shapes, alfas, lambdas]

        validation = Validation(inputs, outputs)
        nn, performance = validation.grid_search(grid, "acc", True)
        acc = performance[0]
        prec = performance[1]
        rec = performance[2]

        print("Search finished.")
        print("Alfa: {} Lambda: {}".format(nn.alfa, nn.lambda_reg))
        print("Accuracy: {}".format(acc))
        print("Precision: {}".format(prec))
        print("Recall: {}".format(rec))
        print("{}".format(nn.weight))

    def dataset_02(self):
        """Training and evaluation for Wine Data Set."""
        norm = Normalization("DataSet_02", 0)
        results = norm.normalize_instances()

        inputs = results[0]
        outputs = results[1]
        s_in = len(inputs[0])
        s_out = len(outputs[0])

        # shapes = [[3, 3, 1], [3, 6, 1], [3, 3, 3, 1], [3, 6, 3, 1]]
        # alfas = [0.0005, 0.005, 0.05, 0.1]
        # lambdas = [0.000001, 0.00001, 0.0001, 0.001]

        
        shapes = [[13, 3, 3], [13, 1, 3], [13, 6, 3]]
        alfas = [1, 0.1, 0.01, 0.001]
        lambdas = [0.001, 0.0001]
        grid = [shapes, alfas, lambdas]

        validation = Validation(inputs, outputs)
        nn, performance = validation.grid_search(grid, "acc", True)
        acc = performance[0]
        prec = performance[1]
        rec = performance[2]

        print("Search finished.")
        print("Alfa: {} Lambda: {}".format(nn.alfa, nn.lambda_reg))
        print("Accuracy: {}".format(acc))
        print("Precision: {}".format(prec))
        print("Recall: {}".format(rec))

    def dataset_03(self):
       """Training and evaluation for Wine Data Set."""
       norm = Normalization("DataSet_03", 9)
       results = norm.normalize_instances()

       inputs = results[0]
       outputs = results[1]
       s_in = len(inputs[0])
       s_out = len(outputs[0])

       # shapes = [[3, 3, 1], [3, 6, 1], [3, 3, 3, 1], [3, 6, 3, 1]]
       # alfas = [0.0005, 0.005, 0.05, 0.1]
       # lambdas = [0.000001, 0.00001, 0.0001, 0.001]

       shapes = [[s_in, 10, s_out], [s_in, 9, 9, s_out]]
       alfas = [0.1, 0.01, 0.001]
       lambdas = [0.01, 0.001, 0.0001]
       grid = [shapes, alfas, lambdas]

       validation = Validation(inputs, outputs)
       validation.max_epochs = 2000

       nn, performance = validation.grid_search(grid, "acc", True)
       acc = performance[0]
       prec = performance[1]
       rec = performance[2]

       print("Search finished.")
       print("Alfa: {} Lambda: {}".format(nn.alfa, nn.lambda_reg))
       print("Accuracy: {}".format(acc))
       print("Precision: {}".format(prec))
       print("Recall: {}".format(rec))

    def dataset_04(self):
        """Training and evaluation for Wine Data Set."""
        norm = Normalization("DataSet_04", 1)
        results = norm.normalize_instances()

        inputs = results[0]
        outputs = results[1]
        s_in = len(inputs[0])
        s_out = len(outputs[0])

        # shapes = [[3, 3, 1], [3, 6, 1], [3, 3, 3, 1], [3, 6, 3, 1]]
        # alfas = [0.0005, 0.005, 0.05, 0.1]
        # lambdas = [0.000001, 0.00001, 0.0001, 0.001]

        
        shapes = [[s_in, 3, s_out], [s_in, 5, s_out],[s_in, 3, 3, s_out]]
        alfas = [1, 0.1, 0.01, 0.001]
        lambdas = [0.001, 0.0001]
        grid = [shapes, alfas, lambdas]

        validation = Validation(inputs, outputs)
        nn, performance = validation.grid_search(grid, "recall", True)
        acc = performance[0]
        prec = performance[1]
        rec = performance[2]

        print("Search finished.")
        print("Alfa: {} Lambda: {}".format(nn.alfa, nn.lambda_reg))
        print("Accuracy: {}".format(acc))
        print("Precision: {}".format(prec))
        print("Recall: {}".format(rec))

    def ds2_test(self):
        norm = Normalization("DataSet_02", 0)
        results = norm.normalize_instances()

        inputs = results[0]
        outputs = results[1]

        validation = Validation(inputs, outputs)
        shape = [13, 6, 3]
        alfa = 0.01
        lambda_reg = 0.001

        nn = NeuralNetwork(shape)
        nn.alfa = 0.01
        nn.lambda_reg = 0.001

        set_train = validation.train_instances
        # epochs, metrics = validation.fit_neural_network(nn, set_train, set_train, test = True)

        # acc = []
        # prec = []
        # rec = []
        # for metric in metrics:
        #     acc.append(metric[0])
        #     prec.append(metric[1][1])
        #     rec.append(metric[2][1])

        # axis_x = np.arange(len(acc))
        # plt.plot(axis_x, acc, label = "Accuracy")
        # # plt.plot(axis_x, prec, label = "Precision")
        # # plt.plot(axis_x, rec, label = "Recall")

        # plt.legend(loc='upper left')
        # plt.show()

        epochs = validation.fit_neural_network(nn, set_train, set_train)
        metrics = validation.test_config(nn)
        print("Accuracy: {}".format(metrics[0]))
        print("Precision: {}".format(metrics[1][1]))
        print("Recall: {}".format(metrics[2][1]))

    def ds3_test(self):
        norm = Normalization("DataSet_03", 9)
        results = norm.normalize_instances()

        inputs = results[0]
        outputs = results[1]

        validation = Validation(inputs, outputs)
        shape = [9, 10, 3]
        alfa = 0.01
        lambda_reg = 0.0001

        nn = NeuralNetwork(shape)
        nn.alfa = alfa
        nn.lambda_reg = lambda_reg

        set_train = validation.train_instances
        validation.max_epochs_min_change = 2000
        validation.max_epochs = 2000
        epochs, metrics = validation.fit_neural_network(nn, set_train, set_train, plot = True, test = True)

        acc = []
        prec = []
        rec = []
        for metric in metrics:
            acc.append(metric[0])
            prec.append(metric[1][1])
            rec.append(metric[2][1])

        axis_x = np.arange(len(acc))
        plt.plot(axis_x, acc, label = "Accuracy")
        plt.plot(axis_x, prec, label = "Precision")
        plt.plot(axis_x, rec, label = "Recall")

        plt.legend(loc='upper left')
        plt.show()

        print("Accuracy: {}".format(metrics[0]))
        print("Precision: {}".format(metrics[1][1]))
        print("Recall: {}".format(metrics[2][1]))
    
    def ds4_test(self):
        norm = Normalization("DataSet_04", 1)
        results = norm.normalize_instances()

        inputs = results[0]
        outputs = results[1]

        validation = Validation(inputs, outputs)
        shape = [31, 5, 2]
        alfa = 0.01
        lambda_reg = 0.0001

        nn = NeuralNetwork(shape)
        nn.alfa = alfa
        nn.lambda_reg = lambda_reg

        set_train = validation.train_instances
        validation.max_epochs_min_change = 200
        validation.max_epochs = 200
        epochs, metrics = validation.fit_neural_network(nn, set_train, set_train, plot = True, test = True)

        acc = []
        prec = []
        rec = []
        for metric in metrics:
            acc.append(metric[0])
            prec.append(metric[1][1])
            rec.append(metric[2][1])

        axis_x = np.arange(len(acc))
        plt.plot(axis_x, acc, label = "Accuracy")
        plt.plot(axis_x, prec, label = "Precision")
        plt.plot(axis_x, rec, label = "Recall")

        plt.legend(loc='upper left')
        plt.show()     

        print("Accuracy: {}".format(metrics[0]))
        print("Precision: {}".format(metrics[1][1]))
        print("Recall: {}".format(metrics[2][1]))

    def ds1_test(self):
        norm = Normalization("DataSet_01", 3)
        results = norm.normalize_instances()

        inputs = results[0]
        outputs = results[1]

        validation = Validation(inputs, outputs)
        shape = [3, 1, 2]
        alfa = 0.1
        lambda_reg = 0

        nn = NeuralNetwork(shape)
        nn.alfa = alfa
        nn.lambda_reg = lambda_reg

        set_train = validation.train_instances
        # validation.max_epochs_min_change = 200
        # validation.max_epochs = 200
        # epochs, metrics = validation.fit_neural_network(nn, set_train, set_train, plot = True, test = True)

        # acc = []
        # prec = []
        # rec = []
        # for metric in metrics:
        #     acc.append(metric[0])
        #     prec.append(metric[1][1])
        #     rec.append(metric[2][1])

        # axis_x = np.arange(len(acc))
        # plt.plot(axis_x, acc, label = "Accuracy")
        # # plt.plot(axis_x, prec, label = "Precision")
        # # plt.plot(axis_x, rec, label = "Recall")

        # plt.legend(loc='upper left')
        # plt.show()

        epochs = validation.fit_neural_network(nn, set_train, set_train)
        metrics = validation.test_config(nn)
        print("Accuracy: {}".format(metrics[0]))
        print("Precision: {}".format(metrics[1][1]))
        print("Recall: {}".format(metrics[2][1]))

if __name__ == "__main__":
    Networks().ds4_test()
