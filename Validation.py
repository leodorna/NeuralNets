from Normalization import Normalization 
from NeuralNetwork import NeuralNetwork 
from math import sqrt
from copy import deepcopy
import numpy as np
import matplotlib.pyplot as plt

MAX_EPOCHS = 1000
MIN_CHANGE = 0.0001
MAX_EPOCHS_MIN_CHANGE = 50

class Validation:
	
	def __init__(self, inputs, outputs):
		self.fold_results = []
		self.k = 5
		self.inputs = inputs
		self.outputs = outputs
		self.steps = 1000
		self.max_epochs = MAX_EPOCHS
		self.min_change = MIN_CHANGE
		self.max_epochs_min_change = MAX_EPOCHS_MIN_CHANGE

		self.test_instances = []
		self.train_instances = []
		self.separate_instances(self.inputs, self.outputs)

	def set_k(self, k):
		self.k = k

	def mean(self, array):
		"""
		Returns the mean of a given list
		"""
		return sum(array)/float(len(array))

	def std_deviation(self, array):
		"""
		Returns the standard deviation of a given list
		"""
		mean = self.mean(array)
		variance = 0
		for data in array:
			variance += (data - mean)**2

		return sqrt(variance/float(len(array)))

	def fold_index(self, k, array):
		"""Returns the k-th fold of a given set

		Parameters
		----------
		k: int
			The fold number
		
		array: list
			The set which contains the fold

		Return
		------
		The index of fold
		"""
		fold_size = len(array)//(self.k)
		return k*fold_size

	def fit_neural_network(self, nn, set_train, set_val, plot = False, test = False):
		"""Train given neural network until minimum error is 
		achieved when testing with validation set.

		Parameters
		----------
		nn : NeuralNetwork
			Neural network to have weights fit.
		
		set_train : list of list of examples
			set_train[0] must be the inputs and set_train[1]
			must be the correct outputs.
		
		set_val : list of list of examples
			set_val[0] must be the inputs and set_val[1]
			must be the correct outputs.

		Return
		------
		Integer indicating epoch when iteration stopped.
		"""
		inp_train = set_train[0]
		out_train = set_train[1]
		inp_val = set_val[0]
		out_val = set_val[1]
		
		errors_cv = []
		errors_train = []
		metrics = []
		min_change_count = 0
		last_error_cv = 10000
		epoch = 0
		count = 0
		while epoch < self.max_epochs:
			for x, y in zip(inp_train, out_train):
				count += 1

				nn.propagate(x)
				nn.delta_backpropagate(y)
				nn.update_weights()
				if test == True and count == 50:
					metrics.append(self.test_config(nn))
					count = 0

			#use training instances to measure the error 
			error_cv = nn.error_j(inp_train, out_train)

			# Stop when many iterations don't minimize error
			if last_error_cv - error_cv < self.min_change:
				min_change_count += 1
			else:
				min_change_count = 0
			last_error_cv = error_cv

			if min_change_count == self.max_epochs_min_change:
				break

			epoch += 1

			if plot == True:
				errors_cv.append(error_cv)
				errors_train.append(nn.error_j(inp_train, out_train))

		if plot == True:
			axis_x = np.arange(epoch)
			plt.plot(axis_x, errors_cv, label = "CV {}".format(nn.alfa))
			plt.plot(axis_x, errors_train, label = "TR {}".format(nn.alfa))
			plt.legend(loc='upper left')
			plt.show()

		if test == True:
			return epoch, metrics

		return epoch

	def separate_instances(self, inputs, outputs, test_size = 0.2):
		"""Separe the training and validation set from the test set

		Parameters
		----------
		inputs: list of lists
			The normalized input instances

		outputs: list of lists
			The normalized instance classes

		"""
		size = int(len(inputs)*test_size)

		self.test_instances = [inputs[:size], outputs[:size]]
		self.train_instances = [inputs[size:], outputs[size:]]
	
	def grid_search(self, grid, opt_metric, print_search = False):
		"""Iterate over given hyperparameters to find neural network model and hyperparameters
		which maximize given metric.

		Parameters
		----------
		grid : list of lists
			Hyperparameters and values to iterate over. Grid[0] corresponds
			to shape, grid[1] corresponds to alfa and grid[2] corresponds to
			lambda_reg.
			Example:
			grid = [[[1,5,1], [1,10,1], [1,10,10,1]], [0.001, 0.01, 0.1], [0.0005, 0.005]]
		
		opt_metric : string
			Metric to be optimized. Values can be: "acc", "precision" or "recall".

		Return
		------
		Return best neural network found.
		"""
		shapes = grid[0]
		alfas = grid[1]
		lambdas = grid[2]
		size = len(shapes) * len(alfas) * len(lambdas)

		performance_best = 0
		nn_best = NeuralNetwork([1, 1])
		performances = (0, 0, 0)
		count = 1
		for shape in shapes:
			for alfa in alfas:
				for lambda_reg in lambdas:
					nn = NeuralNetwork(shape)
					nn.alfa = alfa
					nn.lambda_reg = lambda_reg

					acc, prec, rec = self.cross_validation(nn)
					if print_search == True:
						print("Neural Network {} of {}".format(count, size))
						print("Shape: {} Alfa: {} Lambda: {}".format(shape, alfa, lambda_reg))
						print("Accuracy: {}".format(acc))
						print("Precision: {}".format(prec))
						print("Recall: {}".format(rec))
						print("=========================")

					if opt_metric == "acc":
						if acc[0] >	performance_best:
							performance_best = acc[0]
							performances = (acc, prec, rec)
							nn_best = nn
					elif opt_metric == "precision":
						if prec[0] > performance_best:
							performance_best = prec[0]
							performances = (acc, prec, rec)
							nn_best = nn
					elif opt_metric == "recall":
						if rec[0] > performance_best:
							performance_best = rec[0]
							performances = (acc, prec, rec)
							nn_best = nn
					count += 1
		return nn_best, performances

	def cross_validation(self, nn):
		"""
		Applies the k-fold cross-validation while training the network

		Parameters
		----------
		nn : Neural Network
			Neural network to use during cross validation.

		Return
		------
		Three tuples (accuracy, precision and recall), each containing both average 
		and standard deviation for the specific metric.
		"""
		self.fold_results = []
		train_instances = self.train_instances
		for k in range(0, self.k):
			i_now = self.fold_index(k, train_instances[0])
			i_next = self.fold_index(k+1, train_instances[0])

			#the logic to separate the validation_intances and training instances is below:
			validation_set = [train_instances[0][i_now:i_next], train_instances[1][i_now:i_next]]
			
			if k == 0:
				training_set = [train_instances[0][i_next:], train_instances[1][i_next:]]
			elif k == self.k-1:
				if len(self.inputs)%self.k == 0:
					training_set = [train_instances[0][:i_now], train_instances[1][:i_now]]
				else:
					training_set = [train_instances[0][:i_now] + train_instances[0][i_next:], train_instances[1][:i_now] + train_instances[1][i_next:]]
			else:
				training_set = [train_instances[0][:i_now] + train_instances[0][i_next:], train_instances[1][:i_now] +train_instances[1][i_next:]]
			
			#training with the training instances
			self.fit_neural_network(nn, training_set, training_set, plot=False)

			#We need a confusion matrix for each class
			metricas = []
			for i in range(0, len(self.outputs[0])):
				metricas.append({'VP':0, 'VN': 0, 'FP': 0, 'FN': 0})

			#testing the NN with the test instances and measuring the results 
			for x, y in zip(validation_set[0], validation_set[1]):
				res = nn.propagate(x)
				
				for i in range(0, len(res)):
					metricas[i][self.diagnosis(i, res, y)] += 1

			self.fold_results.append(metricas)

		return (self.get_accuracy(), self.get_recall(), self.get_prec())

	def test_config(self, nn):
		"""Test given neural network against test set.
		
		Return
		------
		(Accuracy, [[Recall Class 0 : n], Recall Macro]. [[Prec Class 0 : n], Prec Macro])

		"""
		metricas = []

		# One confusion matrix for each class
		for i in range(0, len(self.test_instances[1][0])):
			metricas.append({'VP':0, 'VN': 0, 'FP': 0, 'FN': 0})
	

		for x, y in zip(self.test_instances[0], self.test_instances[1]):
			res = nn.propagate(x)
			
			for i in range(0, len(res)):
				metricas[i][self.diagnosis(i, res, y)] +=1

		return (self.get_acc_fold(metricas), self.get_recall_fold(metricas), self.get_prec_fold(metricas))
	
	def diagnosis(self, analized_class, res, y):
		"""
			Classifies the output from the network in VP, VN, FP or FN
		"""
		predicted = np.argmax(res)
		real = np.argmax(y)

		if predicted == real:
			if analized_class == predicted:
				return 'VP'
			else:
				return 'VN'
		else:
			if analized_class == predicted:
				return 'FP'
			elif analized_class == real:
				return 'FN'
			else:
				return 'VN'

	def get_prec_fold(self, metricas):
		"""
		Calculates the precision in a fold
		Returns the precision for each class and the macro precision
		"""
		prec = []
		for i in range(len(metricas)):
			VP = metricas[i]['VP']
			FP = metricas[i]['FP']
			if FP + VP != 0:
				prec.append(VP/(float(FP+VP)))
			else:
				prec.append(0)


		return [prec[0:], self.mean(prec)]

	def get_recall_fold(self, metricas):
		"""
		Calculates the recall in a fold
		Returns the recall for each class and the macro recall
		"""
		recall = []
		for i in range(len(metricas)):
			VP = metricas[i]['VP']
			FN = metricas[i]['FN']
			recall.append(VP/(float(FN+VP)))

		return [recall[0:], self.mean(recall)]

	def get_acc_fold(self, metricas):
		"""
		Calculates the accuracy in a fold
		"""
		acc = 0
		for i in range(len(metricas)):
			VP = metricas[i]['VP']
			acc += VP

		return (acc/float(len(self.test_instances[0])))

	def get_prec_class(self, classe):
		prec = []
		for fold in self.fold_results:
			VP = fold[classe]['VP']
			FP = fold[classe]['FP']
			if FP+VP != 0:
				prec.append(VP/float((FP+VP)))
			else:
				prec.append(1)

		return (self.mean(prec), self.std_deviation(prec))

	def get_recall_class(self, classe):
		recall = []
		for fold in self.fold_results:
			VP = fold[classe]['VP']
			FN = fold[classe]['FN']
			if FN+VP != 0:
				recall.append(VP/float((FN+VP)))
			else:
				recall.append(1)

		return (self.mean(recall), self.std_deviation(recall))

	def get_accuracy(self):
		"""
		Return the average accuracy and standard deviation of the folds 
		"""
		acc = []
		for fold in self.fold_results:
			acertos = 0
			for classe in fold:
				acertos += classe['VP']
			acc.append(acertos/float(self.fold_index(1, self.inputs[len(self.inputs)//5:])))

		return (self.mean(acc), self.std_deviation(acc))

	def get_recall(self):
		"""
		Return the macro recall and standard deviation of the recall of the folds
		"""
		recall_mean = 0
		recall_dev = 0
		size = float(len(self.fold_results[0]))
		for classe in range(0, int(size)):
			recall = self.get_recall_class(classe)
			recall_mean += recall[0]
			recall_dev += recall[1]
		return (recall_mean/size, recall_dev/size)


	def get_prec(self):
		"""
		Return the macro precision and standard deviation of the precision of the folds
		"""
		prec_mean = 0
		prec_dev = 0
		size = float(len(self.fold_results[0]))
		for classe in range(0, int(size)):
			prec = self.get_prec_class(classe)
			prec_mean += prec[0]
			prec_dev += prec[1]

		return (prec_mean/size, prec_dev/size)
"""
def test_fit_nn():
	a = Normalization("DataSet_03", 9)

	print(a.inputs)

	results = a.normalize_instances()
	inputs = results[0]
	outputs = results[1]

	size = len(inputs)
	val = Validation(inputs, outputs)

	set_tr = [inputs[: int(0.7 * size)], outputs[: int(0.7 * size)]]
	set_val = [inputs[int(0.7* size) :], outputs[int(0.7 * size) :]]

	# for i in range(6):
	# 	for j in range(3):
	val.max_epochs = 2000
	val.min_change = 1/(10**(7))
	val.max_epochs_min_change = 100
	nn = NeuralNetwork([len(inputs[0]), 3, 3])
	nn.lambda_reg = 1/(10**(3))
	nn.alfa = 0.01

	print(val.fit_neural_network(nn, set_tr, set_val, plot = True))

if __name__ == "__main__":
	test_fit_nn()
"""
